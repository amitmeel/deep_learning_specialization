# About this specialization:
If you want to break into AI, this Specialization will help you do so. Deep Learning is one of the most highly sought after skills in tech. We will help you become good at Deep Learning.

In five courses, you will learn the foundations of Deep Learning, understand how to build neural networks, and learn how to lead successful machine learning projects. You will learn about Convolutional networks, RNNs, LSTM, Adam, Dropout, BatchNorm, Xavier/He initialization, and more. You will work on case studies from healthcare, autonomous driving, sign language reading, music generation, and natural language processing. You will master not only the theory, but also see how it is applied in industry. You will practice all these ideas in Python and in TensorFlow, which we will teach.

You will also hear from many top leaders in Deep Learning, who will share with you their personal stories and give you career advice.

AI is transforming multiple industries. After finishing this specialization, you will likely find creative ways to apply it to your work.

We will help you master Deep Learning, understand how to apply it, and build a career in AI.



# Projects Overview

You will see and work on case studies in healthcare, autonomous driving, sign language reading, music generation, and natural language processing. You will also build near state-of-the-art deep learning models for several of these applications. In a "Machine Learning flight simulator", you will work through case studies and gain "industry-like experience" setting direction for an ML team.


This [Deep Learning Specialization](https://www.coursera.org/specializations/deep-learning) Contians five courses :

[Course 1 Neural Networks and Deep Learning](https://www.coursera.org/learn/neural-networks-deep-learning/home/welcome)

[Course 2 Improving Deep Neural Networks](https://www.coursera.org/learn/deep-neural-network/home/welcome) 

[Course 3 Structured Machine Learning Projects](https://www.coursera.org/learn/machine-learning-projects/home/welcome) 

[Course 4 Convolutional Neural Networks](https://www.coursera.org/learn/convolutional-neural-networks/home/welcome)

[Course 5 Sequence Models](https://www.coursera.org/learn/nlp-sequence-models/home/welcome) 
